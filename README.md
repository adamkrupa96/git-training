# Git training

- Utwórz folder a w nim trzy katalogi: local_repo, second_local_repo, remote_repo
- Przejdź do folderu local_repo i załóż w nim nowe repozytorium git
  `git init`
- Przejdź do folderu remote_repo i załóż “nagie” zdalne repo
  `git init --bare`

- Wróć do local_repo, stwórz plik o nazwie loremipsum.txt po czym dodaj w nim jedną linie z generatora lorem ipsum.
- Sprawdź status zmian w repozytorium
  `git status`
- Dodaj plik loremipsum.txt do “poczekalni” (staging area)
  `git add loremipsum.txt`
- Sprawdź status zmian ponownie
- Wykonaj commit z opisem przy użyciu parametru
  `git commit -m “YOUR_DESCRIPTION”`
- Sprawdź status
- Ustaw jako zdalne repo wcześniej zainicjalizowane “nagie” repozytorium
  `git remote add origin ../remote`
- Sprawdź aktualnie ustawione zdalne repo
  `git remove -v`
- Wyślij lokalne commity do zdalnego repo
  `git push -u origin master`
  == `git push –set-upstream origin master`
- Dodaj plik .gitignore z rozszerzeniem dla plików .doc
  `echo “\*.doc” > .gitignore`
- Dodaj plik ignored.doc
- Zapisz zmiany, uaktualnij zdalne repo
- Zapisz log zmian w pliku log.txt po czym schowaj go na później
- `git log > log.txt`
- `git stash`
- Dodaj nową linie z lorem ipsum do pliku loremipsum.txt
- Wygeneruj plik diff.txt zawierający różnice z wprowadzonych zmian
- `git diff loremipsum.txt > diff.txt`
- Odłóż na później (stash) dokonane zmiany wraz z dodanym plikiem diff.txt i opisem dla przechowywanego wiersza
- `git stash push –include-untracked -m “DESCRIPTION”`
- Usuń poprzedni wiersz ze stasha
- `git stash list`
- `git stash drop stash@{1}`
- Wyjmij ze stasha zapisany najnowszy stash
- Dokonaj zmiany w pliku loremipsum.txt
- Przejdź do second_local_repo i sklonuj do niego zawartość zdalnego repa
- `git clone ../remote_repo`
- W katalogu second_local_repo/remote_repo dodaj kolejną linię z generatora lorem ipsum
- Zapisz zmiany w lokalnym repo i wyślij je do zdalnego repo (dwoma komendami)
- `git commit -a -m “DESCRIPTION”`
- Idź do katalogu local_repo (gdzie powinna wisieć zmiana na pliku loremipsum.txt I plik diff.txt)
- Załóż nowego brancha
- `git branch my_new_branch`
- Sprawdź czy istnieje
- `git branch`
- Na masterze wrzuć zmiany (ale bez pliku diff.txt) do zdalnego repo (rozwiąż problem – git pull), rozwiąż konflikt, wykonaj merga
- Przejdź na zapisanego wcześniej brancha
- `git checkout my_new_branch`
- Sprawdź jak wygląda stan pliku loremipsum.txt
- Dopisz kolejna linie do tego pliku
- Wrzuć zmiany i plik diff.txt do zdalnego repo
- Wróć na mastera
- Cofnij merge pliku loremipsum.txt I wróc do stanu, który był na repo lokalnym (bez zmian z second_local_repo)
- `git revert -m 1 COMMIT_HASH`
- or `git revert -m 2 COMMIT_HASH` ?
- Dodanie dodatkowego kontentu z generatora do pliku loremipsum.txt I wysłanie zmian na repo zdalne
- Przejście do repa lokalnego second_local_repo I uaktualnienie do najnowszych zmian. Wykonanie merga do mastera z branchem my_new_branch
- `git merge my_new_branch`
- Obejrzenie grafu logów po wykonaniu merga branchy (A DOG)
  `git --all --decorate --oneline –graph`
- Wykonanie `git commit --ammend` dla przykładu użycia
